#!/bin/bash

rm -f 1test.txt 


filename=$(basename $(find -name *cif)) #search file in the same folder
frame=${filename%.*}                    #cut out the format 
probe_rad=1.0

for i in 1000 5000 10000 25000 50000 75000 100000 500000
do


echo network_domod -ha -vol $probe_rad $probe_rad $i $frame.cif	  			>> 1test.txt
START=$(date +%s)
/home/daniele/Programs/bin/network_domod -ha -vol $probe_rad $probe_rad $i $frame.cif 	>> 1test.txt
echo                                                                            	>> 1test.txt
cat $frame.vol                                                    			>> 1test.txt
echo											>> 1test.txt
END=$(date +%s)                                                            		
TIME=$(( $END - $START ))
echo "time(s): $TIME "									>> 1test.txt
echo ----------------------------------------------------------------------------------	>> 1test.txt
echo "$i points done"

done





