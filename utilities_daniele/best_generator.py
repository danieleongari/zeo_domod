#!/usr/bin/env python

import math
import subprocess

filename='test_best'

ratm=2 #atomic radius
rprobe=1 #probe radius

rchax=1.5+ratm    #radius of the accessible channel
rchinax=0.75+ratm #radius of the inaccessible channel
rpax=3+ratm       #radius of the accessible pore
rpinax=rpax       #radius of the inaccessible pore

ABC=[0]*3
ABC[0]=3+2*rpax+3
ABC[1]=3+2*rpax+3+2*rpinax+3
ABC[2]=ABC[0]

abc=[90,90,90]

cpax=[3+rpax,3+rpax,3+rpax]
cpinax=[3+rpax,ABC[1]-(3+rpax),3+rpax]
cchax=[3+rpax,3+rpax,'all']
cchinax=[3+rpax,'all',3+rpax]

n=2   #atoms per angstrom

natoms=0

for a in range(0,ABC[0]*n):
 a=float(a)/float(n)
 for b in range(0,ABC[1]*n):
  b=float(b)/float(n)
  for c in range(0,ABC[2]*n):
   c=float(c)/float(n)
   if ((a-cpax[0])**2+(b-cpax[1])**2+(c-cpax[2])**2)        > rpax**2           and \
      ((a-cpinax[0])**2+(b-cpinax[1])**2+(c-cpinax[2])**2)  > rpinax**2         and \
      ((a-cchax[0])**2+(b-cchax[1])**2)                     > rchax**2          and \
      ((a-cchax[0])**2+(c-cchinax[2])**2)                     > rchinax**2 :
       natoms+=1

ofile=open(filename+'.xyz', 'w+')
print >> ofile, "%d"   %(natoms)
print >> ofile, "CELL: %.5f  %.5f  %.5f  %.3f  %.3f  %.3f  " %(ABC[0],ABC[1],ABC[2],abc[0],abc[1],abc[2])

for a in range(0,ABC[0]*n):
 a=float(a)/float(n)
 for b in range(0,ABC[1]*n):
  b=float(b)/float(n)
  for c in range(0,ABC[2]*n):
   c=float(c)/float(n)
   
   if  ((a-cpax[0])**2+(b-cpax[1])**2+(c-cpax[2])**2)        > rpax**2           and \
       ((a-cpinax[0])**2+(b-cpinax[1])**2+(c-cpinax[2])**2)  > rpinax**2         and \
       ((a-cchax[0])**2+(b-cchax[1])**2)                     > rchax**2          and \
       ((a-cchax[0])**2+(c-cchinax[2])**2)                     > rchinax**2 :
 
      print >> ofile, "%3s %8.3f %8.3f %8.3f "  %('B', a,b,c)
 
ofile.close()

subprocess.call("/home/daniele/Programs/python_my/manage_crystal.py "+filename+".xyz cif", shell=True)
